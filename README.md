# Project Title

This is a Chatroom challenge, designed to test knowledge of back-end web technologies, specifically in
.NET and create back- end products with attention to details, standards, and reusability.

## Getting Started

The RabbitMQ configuration is online to my account (currently active), you can change the configuration in web.config file in the appSettings node.

The application create a log folder to manage error log (this will be updated to DB in next version :) ). You can change this in the same web.config settings.

### Prerequisites

To run this project in your environment you need:

```
* .Net Framework 4.6.1
* SQL Server (local) Database
* Access to write in local disk D: or where you configure the Log folder.
```


## Running the tests

Theres many test base on the main MessageParser and API stock quote.


## Deployment

In the folder \ChatroomWebSetup\Debug there's a file sepup.exe based on Click Next login :). To deploy this app take care about Prerequisites

## Built With

* [ASP .NET MVC](https://dotnet.microsoft.com/apps/aspnet/web-apps) - Web Framework
* [RabbitMQ](https://www.rabbitmq.com/) - Message-Broker

## Versioning

We use [BitBucket](https://bitbucket.org/) for versioning. For the versions available, see the [commits on this repository](https://bitbucket.org/sergio_enc28/chatroomchallenge/commits/). 

## Authors

* **Sergio Encarnación** - *Software Engineer* - [SergioEnc28](https://bitbucket.org/%7Be9ec79de-e4fe-45f4-8cc7-4da6d3ba3306%7D/)