﻿using System;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Configuration;

namespace ChatroomBackend
{
    public class LogFile
    {
        public string IpAddress
        {
            get
            {
                return GetIpAddress();
            }
        }

        public static void LogInFile(string line)
        {
            try
            {
                string date = DateTime.Now.ToString("yyyy-MM-dd").Replace(":", "");

                string filePath = string.Format("{0}{1}.txt", ConfigurationManager.AppSettings["LogFolder"], date);

                using (StreamWriter sw = new StreamWriter(filePath, true) { AutoFlush = true })
                {
                    sw.WriteLine(string.Format("\n{0}/: -------------------------------------------------------------------", DateTime.Now));
                    sw.WriteLine(line);
                }
            }
            catch (Exception ex)
            {
                //Just to simulate this
                Console.WriteLine(ex.Message);
            }
        }

        public static void LogInDatabase(string line)
        {
            /*
             * Pending implementation
             * Only create a new table ErrorTable
            */
        }

        private string GetIpAddress()
        {
            try
            {
                var host = Dns.GetHostEntry(Dns.GetHostName());

                return host.AddressList.FirstOrDefault(x => x.AddressFamily == AddressFamily.InterNetwork).ToString();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return string.Empty;
        }
    }
}
