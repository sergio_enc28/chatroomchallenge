﻿using ChatroomBackend.Models;
using CsvHelper;
using System;
using System.Globalization;
using System.IO;
using System.Net;

namespace ChatroomBackend.Stock
{
    public class StockManager
    {
        public StockCsvModel GetStockPrice(string symbol)
        {
            var item = new StockCsvModel();

            try
            {
                var url = Constants.WebApiStock.Replace(Constants.STOCKSYMBOL, symbol);
                var GetURL = WebRequest.Create(url);
                var page = GetURL.GetResponse().GetResponseStream();
                var sr = new StreamReader(page);
                var csv = sr.ReadToEnd();
                using (TextReader p = new StringReader(csv))
                {
                    var csvReader = new CsvReader(p, new CsvHelper.Configuration.CsvConfiguration(CultureInfo.CurrentCulture) { Delimiter = Constants.DELIMITER });
                    while (csvReader.Read())
                    {
                        item = csvReader.GetRecord<StockCsvModel>();
                        return item;
                    }
                }
            }
            catch (Exception ex)
            {
                item.Symbol = string.Format("Error trying to get Stock for '{0}'. Check the Stock Command.", symbol.ToUpper());
                //Simulate use
                Console.WriteLine(ex.Message);
            }
            
            return item;
        }
    }
}
