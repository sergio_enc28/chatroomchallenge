﻿using ChatroomBackend.Stock;
using ChatroomBackend.Models;
using System;

namespace ChatroomBackend.MessageCenter
{
    public class MessageParser
    {
        public static MessagePayload GetMessageParsed(string message)
        {
            var payload = new MessagePayload
            {
                Action = "Chat",
                Message = message,
                RequiredSave = true,
                BotMessage = false
            };

            try
            {
                if (message.Contains("/") && message.Contains("="))
                {
                    var body = message.Split('=');
                    payload.Action = body[0].ToUpper();
                    payload.Message = body[1];
                    //If Bot response messages wont be save in DB, just change it to false.
                    payload.RequiredSave = true;
                    payload.BotMessage = true;
                }


                if (!payload.BotMessage)
                {
                    return payload;
                }

                // /stock=?
                if (payload.Action.Equals("/STOCK"))
                {
                    var manager = new StockManager();
                    var result = manager.GetStockPrice(payload.Message);

                    //If any exception occures during the API execution Symbol propertie contains the details 
                    payload.Message = result.Symbol.ToLower().StartsWith("error ") ? result.Symbol : string.Format("{0} quote is ${1} per share", result.Symbol.ToUpper(), result.Close);
                }
                else
                {
                    payload.Message = $"Error Invalid Command was received. Check: {message}";
                }
                //Here we could have more Tokens
            }
            catch (Exception ex)
            {
                LogFile.LogInFile(ex.Message);
            }
            
            return payload;
        }
    }
}
