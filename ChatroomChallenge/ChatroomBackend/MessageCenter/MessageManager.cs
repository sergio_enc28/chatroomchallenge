﻿using ChatroomBackend.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ChatroomBackend.MessageCenter
{
    public class MessageManager
    {
        public static void SaveMessage(byte[] bodyMessage)
        {
            try
            {
                var message = JsonConvert.DeserializeObject<Message>(Encoding.UTF8.GetString(bodyMessage));
                if (message != null && !string.IsNullOrEmpty(message.MessageText))
                {
                    MessagePayload request = MessageParser.GetMessageParsed(message.MessageText);
                    message.UserName = request.BotMessage ? "BOT" : message.UserName;
                    message.MessageText = request.Message;

                    if (request.RequiredSave)
                    {
                        using (ChatDBContext db = new ChatDBContext())
                        {
                            db.Messages.Add(message);
                            db.SaveChanges();

                            //Notofy UI
                            var data = new StringContent(JsonConvert.SerializeObject(message), Encoding.UTF8, "application/json");
                            var client = new HttpClient();
                            var result = client.PostAsync("http://localhost:57475/chatroom/notifyclients", data).Result;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogFile.LogInFile(ex.Message);
            }
        }
    }
}
