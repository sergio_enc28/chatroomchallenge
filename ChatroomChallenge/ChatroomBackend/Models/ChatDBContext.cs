namespace ChatroomBackend.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class ChatDBContext : DbContext
    {
        public ChatDBContext()
        { }

        public virtual DbSet<Message> Messages { get; set; }
    }
}