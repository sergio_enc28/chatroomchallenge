﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ChatroomBackend.Models
{
    public class Message
    {
        public int MessageID { get; set; }

        [Required]
        public string MessageText { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime DateSent { get; set; }

        [Required]
        public string UserName { get; set; }
    }
}
