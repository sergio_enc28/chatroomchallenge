﻿using System;

namespace ChatroomBackend.Models
{
    public class StockCsvModel
    {
        public string Symbol { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public double Open { get; set; }
        public double High { get; set; }
        public double Low { get; set; }
        public double Close { get; set; }
        public Int64 Volume { get; set; }

    }
}
