﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatroomBackend.Models
{
    public class MessagePayload
    {
        public string Action { get; set; }
        public string Message { get; set; }
        public bool RequiredSave { get; set; }
        public bool BotMessage { get; set; }
    }
}
