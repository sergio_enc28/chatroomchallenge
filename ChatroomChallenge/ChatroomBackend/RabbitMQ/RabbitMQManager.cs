﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using ChatroomBackend.MessageCenter;
using System.Configuration;

namespace ChatroomBackend.RabbitMQ
{
    public class RabbitMQManager
    {
        static ConnectionFactory factory = new ConnectionFactory()
        {
            HostName = ConfigurationManager.AppSettings["HostName"], //"wasp.rmq.cloudamqp.com",
            UserName = ConfigurationManager.AppSettings["UserName"], //"uvechfur",
            Password = ConfigurationManager.AppSettings["Password"], //"lrQSaahw4ZTauKnRVEGg15lvAdT3fvZQ",
            Port = int.Parse(ConfigurationManager.AppSettings["Port"]), //1883,
            Uri = new Uri(ConfigurationManager.AppSettings["Uri"]) //"amqp://uvechfur:lrQSaahw4ZTauKnRVEGg15lvAdT3fvZQ@wasp.rmq.cloudamqp.com/uvechfur")
        };

        
        public static bool CollectorStatus { get; set; }

        public RabbitMQManager()
        {
            var connection = factory.CreateConnection();
            var channel = connection.CreateModel();
            var consumer = new EventingBasicConsumer(channel);
            consumer.Received += Consumer_Received;
            channel.BasicConsume(queue: ConfigurationManager.AppSettings["Queue"], autoAck: true, consumer: consumer);
        }

        private void Consumer_Received(object sender, BasicDeliverEventArgs e)
        {
            MessageManager.SaveMessage(e.Body);
        }

        public void PublishMessage(byte [] body)
        {
            try
            {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: ConfigurationManager.AppSettings["Queue"],
                                         durable: true,
                                         exclusive: false,
                                         autoDelete: false,
                                         arguments: null);

                    channel.BasicPublish(exchange: ConfigurationManager.AppSettings["Exchange"],
                                         routingKey: ConfigurationManager.AppSettings["RoutingKey"],
                                         basicProperties: null,
                                         body: body);
                }
            }
            catch (Exception ex)
            {
                SaveOffLineOrFaildedMessage(body);
                LogFile.LogInFile(ex.Message);
            }
        }

        private static void SaveOffLineOrFaildedMessage(byte[] body)
        {
            /*
             * Pending implementation to DB
             * Only create a new table MessageFailedTable
            */


        }
    }
}
