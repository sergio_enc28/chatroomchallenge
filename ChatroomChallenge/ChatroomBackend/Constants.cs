﻿namespace ChatroomBackend
{
    public static class Constants
    {
        public const int TOPSELECT = 50;
        public const string DELIMITER = ",";
        public const string STOCKSYMBOL = "STOCK-SYMBOL";
        public static string WebApiStock = "https://stooq.com/q/l/?s=STOCK-SYMBOL&f=sd2t2ohlcv&h&e=csv";
    }
}
