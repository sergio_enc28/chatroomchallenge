﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ChatroomBackend.Stock;
using ChatroomBackend.MessageCenter;

namespace ChatroomChallenge.Tests
{
    [TestClass]
    public class StockTests
    {
        [TestMethod]
        public void CanGetStockPrice()
        {
            var manager = new StockManager();
            var result = manager.GetStockPrice("msft.us");
            Assert.IsFalse(result.Symbol.ToLower().StartsWith("error found"));
        }

        [TestMethod]
        public void ValidateMessageParserText()
        {
            var messageSent = "Hello World";
            var result = MessageParser.GetMessageParsed(messageSent);
            Assert.IsTrue(messageSent.Equals(result.Message));
        }

        [TestMethod]
        public void ValidateMessageParserCommand()
        {
            var messageSent = "/stock=aapl.us";
            var result = MessageParser.GetMessageParsed(messageSent);
            Assert.IsTrue(result.BotMessage);
        }

    }
}
