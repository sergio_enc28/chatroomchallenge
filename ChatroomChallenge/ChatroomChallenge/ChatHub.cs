﻿using System;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace ChatroomChallenge
{
    public class ChatHub : Hub
    {
        public void Send(string name, string message, DateTime date)
        {
            var broadcast = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            broadcast.Clients.All.addNewMessageToPage(name, message, date);
        }
    }
}