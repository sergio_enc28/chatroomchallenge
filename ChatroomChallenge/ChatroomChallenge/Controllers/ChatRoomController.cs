﻿using System;
using ChatroomBackend;
using ChatroomBackend.RabbitMQ;
using ChatroomBackend.Models;
using System.Linq;
using System.Web.Mvc;
using System.Text;
using Newtonsoft.Json;

namespace ChatroomChallenge.Controllers
{
    [Authorize]
    public class ChatRoomController : Controller
    {
        ChatDBContext db = new ChatDBContext();
        RabbitMQManager rabbitMQManager = new RabbitMQManager();
        public ActionResult Index(string text = null)
        {
            var query = (from sms in db.Messages
                         orderby sms.DateSent descending
                         select sms).Take(Constants.TOPSELECT);

            var messages = query.AsEnumerable();

            return View(messages);
        }

        [HttpPost]
        [AllowAnonymous]
        public string NotifyClients(Message msg)
        {
            ChatHub chatHub = new ChatHub();
            chatHub.Send(msg.UserName, msg.MessageText, msg.DateSent);
            return "OK";
        }

        public ActionResult Form()
        {
            return PartialView();
        }

        [HttpPost]
        public JsonResult Send(Message model)
        {
            if (!string.IsNullOrEmpty(model.MessageText))
            {
                var temp = System.Web.HttpContext.Current.User.Identity.Name;
                var body = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(new Message { UserName = temp, MessageText = model.MessageText, DateSent = DateTime.Now }));
                rabbitMQManager.PublishMessage(body);
            }
            return Json("Message Sent");
        }
    }
}