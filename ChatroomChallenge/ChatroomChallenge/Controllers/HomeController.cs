﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChatroomChallenge.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return RedirectToAction("index", "chatroom");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Chatroom";

            return View();
        }

        public ActionResult Contact()
        {
            return View();
        }
    }
}