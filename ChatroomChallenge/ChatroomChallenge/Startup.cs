﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ChatroomChallenge.Startup))]
namespace ChatroomChallenge
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
